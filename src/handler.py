import os
import time

from skill_core_tools.skill_log_factory import SkillLogFactory

from src.omdb_query import OmdbQuery

log = SkillLogFactory.get_logger("skill-ladm-demo")
api_key = os.environ["OMDB_API_KEY"]


def evaluate(payload: dict, _context: dict) -> dict:
    start = time.perf_counter()
    try:
        omdb_query = OmdbQuery(api_key)
        log.info("Request received")
        requested_title = payload["title"]

        return omdb_query.find_by_title(requested_title).__dict__()
    finally:
        stop = time.perf_counter()
        log.info("Handled request in %d ms", (stop - start) * 1000)


def on_started(_context: dict):
    print("on_started triggered")


def on_stopped(_context: dict):
    print("on_stopped triggered!")
