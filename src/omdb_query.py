import base64
from typing import List

import requests
from skill_core_tools.skill_log_factory import SkillLogFactory


class Movie:
    def __init__(self, title, year, poster, rating, plot, actors):
        self._title = title or ""
        self._year = year or 0
        self._poster = poster or ""
        self._rating = rating or ""
        self._plot = plot or ""
        self._actors = actors or ""

    def __dict__(self):
        return {
            'title': self._title,
            'year': int(self._year),
            'poster': self._poster,
            'rating': self._rating,
            'plot': self._plot,
            'actors': self._actors
        }


class OmdbQuery:

    _log = SkillLogFactory.get_logger("omdb-query")

    def __init__(self, api_key: str):
        self._url = "http://www.omdbapi.com/?apikey={0}".format(api_key)

    def find_by_title(self, title: str) -> Movie:
        try:
            r = requests.get(self._url, params={"t": title})
            r.raise_for_status()
            json_response = r.json()
            return Movie(json_response["Title"], json_response["Year"], self._poster_as_base64(json_response['Poster']),
                         self._parse_ratings(json_response["Ratings"]), json_response["Plot"], json_response["Actors"])
        except Exception as e:
            self._log.error("Error while resolving '{}':".format(title), exc_info=e)
            raise Exception("Could not resolve a movie for '{}'".format(title))

    def _poster_as_base64(self, poster_url: str) -> str:
        r = requests.get(poster_url)
        r.raise_for_status()
        return base64.b64encode(r.content).decode()

    def _parse_ratings(self, ratings: List[any]):
        # prefer imdb ratings
        imdb_ratings = filter(lambda rating: rating["Source"] == 'Internet Movie Database', ratings)
        imdb_value = list(map(lambda rating: rating["Value"], imdb_ratings))

        if len(imdb_value) != 1:
            return "None"
        return imdb_value[0]
